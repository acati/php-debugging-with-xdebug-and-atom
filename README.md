# PHP Debugging with XDebug and Atom

Quicken, flesh out, and improve your PHP development with debugging.  
Debugging makes any project that much easier.  No more guessing what variable was set to what.  

Just set a breakpoint, refresh your page, and instantly have access to your script's inner workings.
Step through the code, line by line, and view all local and global variables as they are being set.
Even watch specific variables(by name) so you don't have to hunt them down on every refresh.

What's not to like?

---
## Prerequisites...
There are a few things this guide assumes about you as a developer.

1. You have the latest version of [WAMP](http://wampserver.aviatechno.net/)
2. You have [PHP 7.1](http://wampserver.aviatechno.net/) or greater installed and running
3. You have [Atom](https://atom.io "The World's Best Text Editor")
4. You have some experience with [PHP](https://www.w3schools.com/php/ "PHP 5 Tutorial")

If any of those statements are not true, take the time to make them factual.

---
# **Let's Do This**

1. ## Verifying PHP and XDebug

	If: PHP is apart of the Path system variable already 
	
	- Open a cmd or terminal window
	- Type `php --version`
	
	Else:
	
	- With Wamp running, open your browser and go to 'localhost'.
	- Scroll down and click on *phpinfo()*
	
	Either of the methods above will allow you to verify which version of PHP WAMP is currently running and that XDebug is installed.
	If using the the browser method, XDebug package information can be found near the bottom of the page(packages are sorted alphabetically).
	
	
	If XDebug does not appear, it is most likely not installed.
	A Wamp Update to download XDebug for PHP 7 can be found [here](http://wampserver.aviatechno.net/ "Look for Update XDebug 2.7.0").
	
	
	If: WAMP is not running PHP 7 or greater but you have downloaded the appropriate [update](http://wampserver.aviatechno.net/).
	
	- Left click the WAMP manager menu in you toolbar
	- Click 'PHP'
	- In that submenu click 'Version'
	
	This will display all versions of PHP you have installed; a checkmark will be next to the one that WAMP is currently running. 
	
	Choose the appropriate version of PHP, WAMP will restart running that version of PHP. 
	
	Verify again with your method of choice.  If everything is showing correctly, move to the next step.
	
	---
	
2. ## Reconfiguring PHP

	With your PHP version and XDebug installation verified, we now need to reconfigure XDebug a little bit.
	
	---
	
3. ## Verifying PHP reconfiguration

	Open your browser and traverse to localhost; click *phpinfo()*, and scroll down to the XDebug section.
	
	---
	
4. ## Install Atom packages



# **Happy Debugging :) :knife::beetle::gun:**
